﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour {
	public Transform pauseMenu;
	bool isPaused = false;

	// Use this for initialization
	public void resumeGame(){
		if (isPaused) {
			Time.timeScale = 1;
			isPaused = false;
			pauseMenu.gameObject.SetActive (false);
		}
	}
	public void pauseGame(){
			Time.timeScale = 0;
			isPaused = true;
			pauseMenu.gameObject.SetActive (true);
		}

	
	// Update is called once per frame
	void Update () {
		
	}
}
