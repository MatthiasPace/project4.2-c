﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

	Rigidbody2D rB;
	GameObject target;
	Vector2 directionToTarget;
	public float moveSpeed = 20f;

	// Use this for initialization
	void Start () {
		rB = GetComponent<Rigidbody2D> ();
		target = GameObject.Find ("Character");
	}
	
	// Update is called once per frame
	void Update () {
		directionToTarget = (target.transform.position - transform.position).normalized;
		rB.velocity = new Vector2 (directionToTarget.x * moveSpeed, 0f);
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag.Equals ("Bullet")) {
			SoundManagerScript.enemyHit ();
			ScoreScript.scoreNum += 1;
			TimerScript.timeLeft += 3f;
			Destroy (col.gameObject);
			Destroy (gameObject);
		}
		if (col.gameObject.tag.Equals ("Player")) {
			Destroy (gameObject);
			LivesScript.livesCount -= 1;
		}
	}
}
