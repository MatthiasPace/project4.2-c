﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	public float velocityX = 5f;
	float velocityY = 0f;
	Rigidbody2D bullet;
	// Use this for initialization
	void Start () {
		bullet = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		bullet.velocity = new Vector2 (velocityX, velocityY);
		Destroy (gameObject, 0.75f);
	}
}
