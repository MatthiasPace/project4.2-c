﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {

	float playerPos;
	public string midjump = "n";
	public float speed = 25f;
	public float jump = 30f;
	char lastTurn;
	bool facingRight = true;
	public GameObject BulletRight, BulletLeft;
	Vector2 bulletPos;
	public float fireRate = 0.5f;
	float nextBullet = 0.0f;
	public GameObject PowerText;
	static Text text;
	public static float PowerUpTime = 10f;

	// Use this for initialization
	void Start () {
		text = PowerText.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKeyDown (KeyCode.W) && (midjump == "n")) {
			GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, jump, 0);
			midjump = "y";
		}

		if (Input.GetKey(KeyCode.D))
		{
			transform.Translate(-Vector3.left * speed * Time.deltaTime);
			lastTurn = 'd';
		}

		if (Input.GetKey(KeyCode.A))
		{
			transform.Translate (-Vector3.right * speed * Time.deltaTime);
			lastTurn = 'a';
		}

		if(Input.GetKey(KeyCode.Mouse0) && Time.time > nextBullet){
			nextBullet = Time.time + fireRate;
			fire();
		}
		if ((LivesScript.livesCount == 0)||(TimerScript.timeLeft == 0)) {
			Application.LoadLevel (3);
		}
	}

	void LateUpdate(){
		Vector3 localScale = transform.localScale;
		if (lastTurn == 'd') {
			facingRight = true;
		} else if (lastTurn == 'a') {
			facingRight = false;
		}
	}

	void OnCollisionEnter2D(Collision2D collision){
		Vector3 normal = collision.contacts [0].normal;
		if (normal.y > 0) {
			midjump = "n";
		}
		if (collision.gameObject.tag.Equals ("AmmoLootBox")) {
			Destroy (collision.gameObject);
			StartCoroutine ("PowerUpTimer");
		}
		if (collision.gameObject.tag.Equals ("HealthLootBox")) {
			Destroy (collision.gameObject);
			SoundManagerScript.powerup ();
			LivesScript.livesCount += 1;
		}
	}

	void fire(){
		SoundManagerScript.gunSound ();
		bulletPos = transform.position;
		if (facingRight == true) {
			bulletPos += new Vector2 (+1f, -0.35f);
			Instantiate(BulletRight, bulletPos,Quaternion.identity);
		}
		else{
			bulletPos += new Vector2 (-1f, -0.35f);
			Instantiate(BulletLeft, bulletPos,Quaternion.identity);
		}
	}

	IEnumerator PowerUpTimer(){
		PowerUpTime = 10f;
		SoundManagerScript.powerup ();
		while (PowerUpTime > 0f) {
			yield return new WaitForSeconds(1);
			PowerUpTime--;
			text.text = "Fire rate increased";
			fireRate = 0.2f;
		}
		text.text = "";
		StopCoroutine ("PowerUpTimer");
		fireRate = 0.5f;
	}
}
